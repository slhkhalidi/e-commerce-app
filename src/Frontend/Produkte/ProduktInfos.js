import { React, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addProdukt, setProduktMenge } from "../ReduxToolkit/WarenSlice";
import {
  BsPlusCircleFill,
  BsDashCircleFill,
  BsHeartFill,
} from "react-icons/bs";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./Produkte.css";

toast.configure();
function ProduktInfos() {
  let [menge, setMenge] = useState(1);
  const dispatch = useDispatch();
  const globalState = useSelector((state) => state);
  const myProduct = globalState.waren.produktInfos;
  const warenProdukte = globalState.waren.warenProdukte;
  let navigate = useNavigate();

  const plus = () => {
    setMenge(menge + 1);
  };
  const minus = (menge) => {
    if (menge > 1) {
      setMenge(menge - 1);
    }
  };

  useEffect(() => {
    dispatch(setProduktMenge(menge));
  }, [menge]);

  const toWarenkorb = () => {
    if (menge >= 1) {
      dispatch(addProdukt(myProduct));
      toast.success("Der Artikel wurde erfolgreich in den Warenkorb gelegt", {
        position: "bottom-center",
        autoClose: 1500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  return (
    <div className="produktInfoBox">
      <div>
        <img className="produktInfoBild" src={myProduct.image} />
      </div>
      <div className="produktInformationen">
        <h1>{myProduct.name}</h1>
        <br />
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
          sollicitudin sodales hendrerit. Pellentesque quis metus vel dui
          pretium porta ac sed lacus. Nullam non libero non mauris malesuada
          volutpat. In efficitur ultrices est nec scelerisque. Nulla consectetur
          tincidunt purus, eget porta leo tristique vel. In hac habitasse platea
          dictumst. Quisque condimentum odio ac tortor fermentum efficitur.
        </p>
        <h2>{myProduct.preis} €</h2>
        <span className="steuerUndVersand">
          inkl. MwSt. zzgl. Versandkosten
        </span>
        <span className="steuerUndVersand">
          Sofort versandfertig, Lieferzeit ca. 1-3 Werktage
        </span>
        <div className="produktInfosButton">
          <div className="mengeBox">
            <BsPlusCircleFill className="plus" onClick={() => plus()} />
            <span className="menge">{menge} Stk.</span>
            <BsDashCircleFill className="minus" onClick={() => minus(menge)} />
          </div>
          <div>
            <button
              className="toWarenBtn btn btn-success"
              onClick={() => toWarenkorb()}
            >
              In den Warenkorb
            </button>
          </div>
        </div>
        <button
          onClick={() => navigate("/")}
          type="button"
          className="shopenBtn btn btn-primary"
        >
          Weiter einkaufen
        </button>
      </div>
    </div>
  );
}

export default ProduktInfos;
