import { React } from "react";
import produkt from "../Bilder/testProdukt.webp";
import produkt2 from "../Bilder/produkt2.jpeg";
import "./Produkte.css";
import { useDispatch, useSelector } from "react-redux";
import { sendProduktInfo } from "../ReduxToolkit/WarenSlice";
import { useNavigate } from "react-router-dom";

function Gesicht() {
  const dispatch = useDispatch();
  const globalState = useSelector((state) => state);
  const navigate = useNavigate();

  const gesichtProdukte = [
    {
      name: "Pharmos Natur",
      preis: 10.0,
      menge: 0,
      image: produkt,
    },
    {
      name: "produke B",
      preis: 20,
      menge: 0,
      image: produkt2,
    },
    {
      name: "produkt C",
      preis: 100,
      menge: 0,
      image: produkt,
    },
    {
      name: "produkt D",
      preis: 10,
      menge: 0,
      image: produkt,
    },
    {
      name: "produkt C",
      preis: 100,
      menge: 0,

      image: produkt,
    },
    {
      name: "produkt C",
      preis: 100,
      menge: 0,
      image: produkt,
    },
    {
      name: "produkt C",
      preis: 100,
      menge: 0,
      image: produkt,
    },
    {
      name: "produkt C",
      preis: 100,
      menge: 0,
      image: produkt,
    },
  ];

  const getProductInfos = (produkt) => {
    dispatch(sendProduktInfo(produkt));
    navigate("/infos");
  };

  return (
    <div className="produktenBox">
      {gesichtProdukte.map((elem) => {
        return (
          <div onClick={() => getProductInfos(elem)} className="produktBox">
            <img className="produktImage" src={elem.image} alt="#" />
            <span className="titles">{elem.name}</span>
            <span className="titles">{elem.preis} €</span>
            <span>inkl. MwSt zzgl. Versandkosten</span>
          </div>
        );
      })}
    </div>
  );
}

export default Gesicht;
