import { createSlice } from "@reduxjs/toolkit";

 export const authSlice = createSlice( {
    name: "register",
    initialState: {
        users: []
    },
    reducers: {
        addUser: (state,action)=> {
            const user = action.payload;
            state.users.push(user);
          }
    }
    }
 );

export const {addUser} = authSlice.actions;

export default authSlice.reducer; 

