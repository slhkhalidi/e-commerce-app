import { createSlice } from "@reduxjs/toolkit";
import Merkzettel from "../Merkzettel/Merkzettel";

export const MerkzettelSlice = createSlice({
  name: "merkzettel",
  initialState: {
    merkzettel: localStorage.getItem("merkzettel")
      ? JSON.parse(localStorage.getItem("merkzettel"))
      : [],
  },
  reducers: {
    addToMerkzettel: (state, action) => {
      let newArray = [...state.merkzettel];
      const produkt = action.payload;
      newArray.push(produkt);
      state.merkzettel = [...newArray];
    },
    removeFromMerkzettel: (state, action) => {
      const produktIndex = action.payload;
      const myArray = [...state.merkzettel];
      myArray.splice(produktIndex, 1);
      state.merkzettel = [...myArray];
    },
  },
});

export const { addToMerkzettel, removeFromMerkzettel } =
  MerkzettelSlice.actions;

export default Merkzettel.reducer;
