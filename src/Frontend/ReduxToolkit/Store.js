import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./AuthSlice.js";
import warenReducer from "./WarenSlice.js";
import merkzettekReducer from "./MerkzettelSlice";

export default configureStore({
  reducer: {
    auth: authReducer,
    waren: warenReducer,
    merkzettel: merkzettekReducer,
  },
});
