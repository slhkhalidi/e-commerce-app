import { createSlice } from "@reduxjs/toolkit";

export const warenSlice = createSlice({
  name: "warenkorb",
  initialState: {
    warenProdukte: localStorage.getItem("warenProdukte")
      ? JSON.parse(localStorage.getItem("warenProdukte"))
      : [],
    produktInfos: {
      name: null,
      preis: null,
      menge: null,
      image: null,
    },
    toPay: null,
  },
  reducers: {
    addProdukt: (state, action) => {
      let firstArray = [...state.warenProdukte];
      const produkt = action.payload;
      firstArray.push(produkt);
      state.warenProdukte = [...firstArray];
      localStorage.setItem(
        "warenProdukte",
        JSON.stringify(state.warenProdukte)
      );
    },

    removeProdukt: (state, action) => {
      let secondArray = [...state.warenProdukte];
      const index = action.payload;
      secondArray.splice(index, 1);
      state.warenProdukte = [...secondArray];
      localStorage.setItem(
        "warenProdukte",
        JSON.stringify(state.warenProdukte)
      );
    },

    change: (state, action) => {
      state.warenProdukte = [...action.payload];
      localStorage.setItem(
        "warenProdukte",
        JSON.stringify(state.warenProdukte)
      );
    },

    sendProduktInfo: (state, action) => {
      let produkt = action.payload;
      state.produktInfos = produkt;
    },
    setProduktMenge: (state, action) => {
      const menge = action.payload;
      state.produktInfos.menge = menge;
    },
    setToPay: (state, action) => {
      state.toPay = action.payload;
    },
  },
});

export const {
  change,
  setToPay,
  addProdukt,
  removeProdukt,
  sendProduktInfo,
  setProduktMenge,
} = warenSlice.actions;

export default warenSlice.reducer;
