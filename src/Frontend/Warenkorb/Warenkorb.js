import { React, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate, Link } from "react-router-dom";
import { BsChevronRight, BsFillExclamationTriangleFill } from "react-icons/bs";
import "./Warenkorb.css";
import WarenProdukte from "./WarenProdukte";
import Payment from "../Payment/Payment";
import Footer from "../Home/Footer/Footer";
import { change } from "../ReduxToolkit/WarenSlice";

function Warenkorb() {
  const navige = useNavigate();
  const dispatch = useDispatch();
  const globalState = useSelector((state) => state);
  let warenProdukte = globalState.waren.warenProdukte;
  let pay = globalState.waren.toPay;
  let [netto, setNetto] = useState(0);
  let [MwSt, setMwSt] = useState(0);

  useEffect(() => {
    let nett = pay / 1.19;
    let mwst = pay - nett;
    setNetto(nett);
    setMwSt(mwst);
  }, [pay]);

  return (
    <div>
      {warenProdukte.length <= 0 ? (
        <div className="noProductBox-first">
          <div className="noProductBox">
            <BsFillExclamationTriangleFill className="rekIcon" />
            <span className="noProduct">
              Sie haben keine Artikel im Warenkorb
            </span>
          </div>
          <button
            onClick={() => navige("/")}
            type="button"
            className="toShopBtn btn btn-primary"
          >
            Zum Shop
          </button>
        </div>
      ) : (
        <div className="warenDiv">
          <div className="warenKorb">
            <div className="firstElem">
              <h4 className="titles">Artikel</h4>
            </div>
            <div className="secondElem">
              <h4 className="titles">Anzahl</h4>
              <h4 className="titles">Stückpreis</h4>
              <h4 className="titles">Summe</h4>
            </div>
          </div>
          <hr />
          <WarenProdukte />
          <hr />
          <div className="warenKasse">
            <div className="wkFirst">
              <input
                className="gutschein"
                type="text"
                placeholder="Gutschein-Code eingeben..."
              />
              <BsChevronRight className="gutscheinbtn" />
            </div>

            <div className="bigSecondDiv">
              <div className="wkSeconddiv">
                <div className="wkSecond">
                  <span>Summe:</span>
                  <span>Versandkosten:</span>
                  <span className="summe">Gesamtsumme:</span>
                </div>
                <div className="wkSecond">
                  <span>{pay} €</span>
                  <span>kostenlos</span>
                  <span className="summe">{pay} €</span>
                </div>
              </div>
              <Payment />
            </div>
          </div>
        </div>
      )}
      <Footer />
    </div>
  );
}

export default Warenkorb;
