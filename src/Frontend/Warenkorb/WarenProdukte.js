import { React, useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { change, removeProdukt, setToPay } from "../ReduxToolkit/WarenSlice";
import {
  BsTrashFill,
  BsPlusCircleFill,
  BsDashCircleFill,
} from "react-icons/bs";
import "./Warenkorb.css";

function WarenProdukte(props) {
  const dispatch = useDispatch();
  const globalState = useSelector((state) => state);
  let warenProdukte = globalState.waren.warenProdukte;
  let [menge, setMenge] = useState(0);

  useEffect(() => {
    let toPay = 0;
    let sum = 0;
    warenProdukte.map((product) => {
      sum = product.preis * product.menge;
      toPay = toPay + sum;
    });
    dispatch(setToPay(toPay));
  }, [warenProdukte]);

  const remove = (index) => {
    dispatch(removeProdukt(index));
  };

  function addArtikel(product) {
    let array = [...warenProdukte];
    for (let i = 0; i < array.length; i++) {
      if (array[i] === product) {
        let obj = { ...array[i] };
        obj.menge++;
        array[i] = { ...obj };
        dispatch(change(array));
        break;
      }
    }
  }

  function removeArtikel(product) {
    let array = [...warenProdukte];
    for (let i = 0; i < array.length; i++) {
      if (array[i] === product && array[i].menge > 1) {
        let obj = { ...array[i] };
        obj.menge--;
        array[i] = { ...obj };
        dispatch(change(array));
        break;
      }
    }
  }

  return warenProdukte.map((elem, idx) => {
    return (
      <div className="warenKorb">
        <div className="firstElem">
          <img className="warenImg" src={elem.image} alt="#" />
          <span>{elem.name}</span>
        </div>
        <div className="secondElem">
          <span>
            <BsPlusCircleFill
              className="plus"
              onClick={() => addArtikel(elem)}
            />{" "}
            {elem.menge} Stk.{" "}
            <BsDashCircleFill
              className="minus"
              onClick={() => removeArtikel(elem)}
            />
          </span>
          <span>{elem.preis} €</span>
          <span>{elem.preis * elem.menge} €</span>
        </div>
        <BsTrashFill className="trash" onClick={() => remove(idx)} />
      </div>
    );
  });
}

export default WarenProdukte;
