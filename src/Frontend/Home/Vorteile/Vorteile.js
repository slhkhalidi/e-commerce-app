import React from "react";
import "./Vorteile.css";
import { BsCheckLg } from "react-icons/bs";
import { BsPatchCheckFill } from "react-icons/bs";

function Vorteile() {
  return (
    <div className="vorteile" id="Vorteile">
      <h1>Hi! Wir haben einen Fokus -Dich</h1>
      <span>
        Bio bedeutet: Du beginnst deinen Tag mit dem Gefühl, die richtige
        Entscheidung getroffen zu haben.
      </span>
      <br />
      <br />
      <h2>Ihre Vorteile bei uns:</h2>
      <div className="bonus">
        <div className="gridBoxes">
          <BsPatchCheckFill className="checkLogo" />
          Unsere große Auswahl an echter Naturkosmetik stillt die
          unterschiedlichsten Bedürfnisse. Sicher finden auch Sie genau die
          Produkte, die wie für Sie gemacht sind.
        </div>
        <div className="gridBoxes">
          <BsPatchCheckFill className="checkLogo" />
          Auch in Zeiten der Corona-Krise bemühen wir uns um schnellen Versand.
          In dringenden Fällen stehen Ihnen zudem mehrere
          Express-Versand-Optionen zur Verfügung.
        </div>
        <div className="gridBoxes">
          <BsPatchCheckFill className="checkLogo" />
          Sie sind fantastisch einzigartig. Daher bieten wir Ihnen eine
          individuelle und persönliche Beratung. Über unsere Kontakt-Seite
          können Sie uns jederzeit ansprechen.
        </div>
        <div className="gridBoxes">
          <BsPatchCheckFill className="checkLogo" />
          Bei jeder Lieferung legen wir Ihnen kostenfrei Proben dazu. Wenn Sie
          uns im Kommentarfeld spezielle Wünsche nennen, versuchen wir diese
          nach Möglichkeit zu erfüllen.
        </div>
      </div>
      <br />
      <h2>Viel Spaß beim Stöbern & Entdecken!</h2>
    </div>
  );
}

export default Vorteile;
