import React from "react";
import "./Home.css";
import { connect } from "react-redux";
import Sliders from "./Sliders/Sliders.js";
import Vorteile from "./Vorteile/Vorteile.js";
import Footer from "./Footer/Footer.js";
import Gesicht from "../Produkte/Gesicht.js";
import Newsletter from "./Newsletter/Newsletter";

function Home(props) {
  return (
    <div>
      <Sliders />
      <Vorteile />
      <Gesicht />
      <Newsletter />
      <Footer />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
