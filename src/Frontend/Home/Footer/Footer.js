import React from "react";
import "./Footer.css";
import { BsFacebook, BsInstagram, BsTwitter, BsYoutube } from "react-icons/bs";

function Footer() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="row">
          <div className="footer-col ">
            <h3 className="titles">Natürgerie</h3>
            <hr className="ligne" />
            <span>Über uns</span>
            <span>FAQ</span>
            <span>Blog</span>
            <span>Presse</span>
            <span>Jobs</span>
          </div>
          <div className="footer-col">
            <h3 className="titles">Informationen</h3>
            <hr className="ligne" />
            <span>AGB</span>
            <span>Widerrufsbelehrung</span>
            <span>Cookie-Einstellungen</span>
            <span>Datenschutz</span>
            <span>Impressum</span>
          </div>
          <div className="footer-col">
            <h3 className="titles">Entdecke</h3>
            <hr className="ligne" />
            <span>Asiatisch</span>
            <span>Indisch</span>
            <span>Afrikanisch</span>
            <span>Ernährung</span>
            <span>Körper</span>
          </div>
          <div className="footer-col">
            <h3 className="titles">Folge uns</h3>
            <hr className="ligne" />
            <div>
              <BsFacebook className="social-media" />
              <BsInstagram className="social-media" />
              <BsTwitter className="social-media" />
              <BsYoutube className="social-media" />
            </div>
          </div>
        </div>
      </div>
      <div className="copyright">
        <span>
          © Copyright 2022 | Natural Beauty - Alle Rechte vorbehalten.
        </span>
      </div>
    </footer>
  );
}

export default Footer;
