import React from "react";
import "./Newsletter.css";
import { BsChevronRight } from "react-icons/bs";

function Newsletter() {
  return (
    <div className="newsletter">
      <div className="newsletter-container">
        <h1>Verpasse nichts mehr & melde dich zum Newsletter an !</h1>
        <br />
        <span>
          Wir benachrichtigen dich nur, wenn es Neuigkeiten & Rabatte gibt!
        </span>
        <span>Trage jetzt deine E-mail Adresse ein</span>
        <br />
        <div>
          <input type="email" placeholder="Email..." className="set-email" />
          <BsChevronRight className="gutscheinbtn" />
        </div>
      </div>
    </div>
  );
}

export default Newsletter;
