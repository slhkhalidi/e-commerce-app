import React from "react";
import logo from "./logoTest.webp";
import "./Navbar.css";
import {
  BsFillPersonFill,
  BsFillSuitHeartFill,
  BsCartFill,
  BsSearch,
} from "react-icons/bs";
import { Link, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

function Navbar() {
  const globalState = useSelector((state) => state);
  const navigate = useNavigate();
  const warenProdukteFromLocal = JSON.parse(
    localStorage.getItem("warenProdukte")
  );

  return (
    <div>
      <div className="navbar">
        <img
          className="logo"
          src={logo}
          alt="#"
          onClick={() => navigate("/")}
        />
        <div className="iconsBoxes">
          <div className="iconBox">
            <BsFillPersonFill className="icon" />
            <Link className="links" to="/login">
              <span>MEIN KONTO</span>
            </Link>
          </div>

          <div className="iconBox">
            <BsFillSuitHeartFill className="icon" />
            <Link to="/merkzettel" className="links">
              MERKZETTEL
            </Link>
          </div>

          <div className="iconBox">
            <div className="warenBox">
              <BsCartFill className="icon" />
              <span className="warenMenge">
                {warenProdukteFromLocal.length}
              </span>
            </div>
            <Link className="links" to="/warenkorb">
              <span>WARENKORB</span>
            </Link>
          </div>
        </div>
      </div>

      <div className="categories">
        <Link className="links" to="/gesicht">
          Gesicht
        </Link>
        <span className="links">Körper</span>
        <span className="links">Haare</span>
        <span className="links">Zähne</span>
        <span className="links">Düfte</span>
        <span className="links">Ernährung</span>
        <span className="links">Asiatisch</span>
        <span className="links">Indisch</span>
        <span className="links">Afrikanisch</span>
      </div>
      <hr />
    </div>
  );
}

export default Navbar;
