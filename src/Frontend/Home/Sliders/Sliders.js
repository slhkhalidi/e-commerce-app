import React from "react";
import Slider from "react-slick";
import "./Sliders.css";
import test5 from "./test5.avif";
import test1 from "./test1.jpeg";
import test2 from "./test2.jpeg";
import test3 from "./test3.jpeg";
import test4 from "./test4.jpeg";
import { BsArrowDownCircleFill } from "react-icons/bs";
import { Link as Scroll } from "react-scroll";

function Sliders() {
  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 200,
    autoplaySpeed: 2000,
    cssEase: "linear",
  };

  return (
    <Slider className="sliders" {...settings}>
      <div>
        <div className="slider">
          <div className="desc-box">
            <h1 className="description titles">
              Herzlich willkommen bei Natural Beauty
            </h1>
            <Scroll to="Vorteile" smooth={true}>
              <BsArrowDownCircleFill className="iconUs" />
            </Scroll>
          </div>
          <div className="foto-box">
            <img className="foto" src={test5} alt="#" />
          </div>
        </div>
      </div>
      <div>
        <div className="slider">
          <div className="desc-box">
            <h1 className="description ">Purer Körper</h1>
            <hr />
            <span>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod{" "}
            </span>
            <br />
            <button className="btn btn-success entdecken">Entdecken</button>
          </div>
          <div className="foto-box">
            <img className="foto" src={test2} alt="#" />
          </div>
        </div>
      </div>
      <div>
        <div className="slider">
          <div className="desc-box">
            <h1 className="description ">Purer Haare</h1>
            <hr />
            <span>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod{" "}
            </span>
            <br />

            <button className="btn btn-success entdecken">Entdecken</button>
          </div>
          <div className="foto-box">
            <img className="foto" src={test3} alt="#" />
          </div>
        </div>
      </div>
      <div>
        <div className="slider">
          <div className="desc-box">
            <h1 className="description ">Pure Schönheit</h1>
            <hr />
            <span>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod{" "}
            </span>
            <br />

            <button className="btn btn-success entdecken">Entdecken</button>
          </div>
          <div className="foto-box">
            <img className="foto" src={test4} alt="#" />
          </div>
        </div>
      </div>
    </Slider>
  );
}

export default Sliders;
