import { React, useState, useEffect } from "react";
import "./Login.css";
import { useSelector, useDispatch } from "react-redux";
import { addUser } from "../ReduxToolkit/AuthSlice.js";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { BsCheckCircleFill } from "react-icons/bs";
import Footer from "../Home/Footer/Footer.js";

function Login() {
  const navigate = useNavigate();
  const globalState = useSelector((state) => state);
  const dispatch = useDispatch();

  const [checkUserName, setcheckUserName] = useState("");
  const [checkUserPass, setcheckUserPass] = useState("");
  const [isValider, setValider] = useState(false);

  useEffect(() => {
    let actualState = localStorage.getItem("isValider");
    setValider(JSON.parse(actualState));
  }, []);

  useEffect(() => {
    localStorage.setItem("isValider", JSON.stringify(isValider));
  });

  async function checkUser(userName, password) {
    if (userName !== "" && password !== "") {
      const request = await fetch("http://localhost:3001/login", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          userName,
          password,
        }),
      });
      const data = await request.json();
      if (data.status === 200 && data.user) {
        setValider(true);
        navigate("/");
      } else {
        alert("Bitte setzen Sie korrekte Eingaben ein");
      }
    }
  }

  function abmelden() {
    setValider = false;
  }

  return (
    <div className="loginBox">
      {isValider === false ? (
        <div>
          <h2>Du bist schon angemeldet</h2>
          <button onClick={() => abmelden()} className="btn btn-danger">
            abmelden
          </button>
        </div>
      ) : (
        <div className="loginBox">
          <div className="login">
            <h2 className=" anmelden titles">Anmelden</h2>
            <span>Nutzername</span>
            <input
              className="inputBox"
              placeholder="Gib dein Nutzername ein..."
              onChange={(e) => setcheckUserName(e.target.value)}
            />
            <div className="passwort">
              <span>Passwort</span>
              <span>Passwort vergessen?</span>
            </div>
            <input
              type="password"
              className="inputBox"
              placeholder="Gib dein Passwort ein..."
              onChange={(e) => setcheckUserPass(e.target.value)}
            />
            <button
              type="button"
              className="btn btn-success"
              onClick={() => checkUser(checkUserName, checkUserPass)}
            >
              Anmelden
            </button>
            <br />
            <div className="oder">
              <span>oder</span>
            </div>
            <br />
            <Link to="/registieren" type="button" className=" btn btn-primary">
              Registieren
            </Link>
          </div>
          <div className="mein-vorteile">
            <h4 className="titles">Meine Vorteile:</h4>
            <span>
              <BsCheckCircleFill className="" /> Schnelles Einkaufen
            </span>
            <span>
              <BsCheckCircleFill /> Speichern Sie Ihre Benutzerdaten und
              Einstellungen
            </span>
            <span>
              <BsCheckCircleFill /> Einblick in Ihre Bestellungen inkl.
              Sendungsauskunft
            </span>
            <span>
              <BsCheckCircleFill /> Verwalten Sie Ihr Newsletter-Abo
            </span>
          </div>
        </div>
      )}

      <Footer />
    </div>
  );
}

export default Login;
