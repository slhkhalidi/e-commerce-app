import { React, useState } from "react";
import "./Login.css";
import { useNavigate } from "react-router-dom";
import Footer from "../Home/Footer/Footer.js";

function Register() {
  const navigate = useNavigate();

  const [vorname, setVorname] = useState("");
  const [nachname, setNachname] = useState("");
  const [email, setEmail] = useState("");
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  function checkAndSetVorname(vorname) {
    if (vorname !== "") {
      let checking = vorname.match(/\d+/g);
      if (checking != null) {
        setVorname("");
      } else {
        setVorname(vorname);
      }
    }
  }

  function checkAndSetNachname(nachname) {
    if (nachname !== "") {
      const checking = nachname.match(/\d+/g);
      if (checking == null) {
        setNachname(nachname);
      } else {
        setNachname("");
      }
    }
  }

  async function setUsers(vorname, nachname, email, userName, password) {
    if (
      vorname !== "" &&
      nachname !== "" &&
      email !== "" &&
      userName !== "" &&
      password !== ""
    ) {
      const request = await fetch("http://localhost:3001/registieren", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          vorname,
          nachname,
          email,
          userName,
          password,
        }),
      });
      navigate("/");
    } else {
      alert("Bitte setzen Sie korrekte Eingaben ein");
    }
  }

  return (
    <div className="loginBox">
      <div className="register">
        <h2 className="anmelden titles">Konto erstellen</h2>
        <span>Vorname</span>
        <input
          placeholder="Gib deine Vorname ein..."
          onChange={(event) => checkAndSetVorname(event.target.value)}
        />
        <span>Nachname</span>
        <input
          placeholder="Gib deine Nachname ein..."
          onChange={(event) => checkAndSetNachname(event.target.value)}
        />
        <span>E-mail</span>
        <input
          type="email"
          placeholder="Gib deine E-mail ein..."
          onChange={(event) => setEmail(event.target.value)}
        />
        <span>Nutzername</span>
        <input
          placeholder="Gib deine Nutzername ein..."
          onChange={(event) => setUserName(event.target.value)}
        />
        <span>Passwort</span>
        <input
          type="password"
          placeholder="Gib deine Passwort ein..."
          onChange={(event) => setPassword(event.target.value)}
        />
        <div className="datenschutz">
          <input type="checkbox" value="1" />
          <span>
            Ich habe die <a>Datenschutzbestimmungen</a> zur Kenntnis genommen.
          </span>
        </div>
        <button
          type="button"
          className="registerBtn btn btn-primary"
          onClick={() => setUsers(vorname, nachname, email, userName, password)}
        >
          Konto erstellen
        </button>
      </div>
      <Footer />
    </div>
  );
}

export default Register;
