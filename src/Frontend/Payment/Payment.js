import { React, useState, useEffect } from "react";
import StripeCheckout from "react-stripe-checkout";
import { useSelector } from "react-redux";
import "../Warenkorb/Warenkorb.css";
import { Link } from "react-router-dom";
import { BsChevronRight } from "react-icons/bs";

function Payment() {
  const globalState = useSelector((state) => state);
  const preis = globalState.waren.toPay;
  const myProducts = globalState.waren.warenProdukte;
  const key =
    "pk_test_51KlCYpD48uUpExQdYEmVWaGQLWiv2pitLpCu3yzgNZ0jtgTPw3rHzgrOyiL5F2jSH2lVlY2cUtrITJwsoFcY7P8m009pAG7WuD";

  async function onToken(token) {
    try {
      const request = await fetch("http://localhost:3001/payment", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          tokenId: token.id,
          amount: preis * 100,
          produkte: myProducts,
          user: token,
        }),
      });
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className="warenBtn">
      <StripeCheckout
        name="Salah Test"
        shippingAddress
        billingAddress={true}
        description="Test"
        stripeKey={key}
        token={onToken}
        amount={preis * 100}
      >
        <button type="button" className=" wb btn btn-success">
          Zur Kasse <BsChevronRight />
        </button>
      </StripeCheckout>
    </div>
  );
}

export default Payment;
