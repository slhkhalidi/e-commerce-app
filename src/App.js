import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./Frontend/ReduxToolkit/Store.js";
import Login from "./Frontend/Login/Login.js";
import Navbar from "./Frontend/Home/Navbar/Navbar.js";
import Home from "./Frontend/Home/Home.js";
import Register from "./Frontend/Login/Register.js";
import Warenkorb from "./Frontend/Warenkorb/Warenkorb.js";
import Gesicht from "./Frontend/Produkte/Gesicht";
import ProduktInfos from "./Frontend/Produkte/ProduktInfos";
import Payment from "./Frontend/Payment/Payment";
import Merkzettel from "./Frontend/Merkzettel/Merkzettel";

function App() {
  return (
    <BrowserRouter>
      <div>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/registieren" element={<Register />} />
          <Route path="/warenkorb" element={<Warenkorb />} />
          <Route path="/gesicht" element={<Gesicht />} />
          <Route path="/infos" element={<ProduktInfos />} />
          <Route path="/payment" element={<Payment />} />
          <Route path="/merkzettel" element={<Merkzettel />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default function AppRedux() {
  return (
    <Provider store={store}>
      <div>
        <App />
      </div>
    </Provider>
  );
}
