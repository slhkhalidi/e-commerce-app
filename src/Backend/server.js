const express = require("express");
const cors = require("cors");
const server = express();
const mongoose = require("mongoose");
const model = require("./mongoDb.js");
const jwt = require("jsonwebtoken");
const stripe = require("stripe")(
  "sk_test_51KlCYpD48uUpExQd4Nl1GWfaaZsJLqayrP1qCfSpNTWbrczxSPCFfdt7aRHJgq1sEtHN827A2yu8laCpWuhE1Ysd00FynmSLLw"
);
const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 587,
  secure: false,
  auth: {
    user: "33db5971af1359",
    pass: "0c8844033e6d81",
  },
});

// cors() is a Middelware if you use different Port FE BE //
server.use(cors());
server.use(express.json());
server.listen(3001, () => {
  console.log("Server is Running");
});

// MongoDB //
mongoose.connect(
  "mongodb+srv://shop:1998@cluster0.c6bzk.mongodb.net/shopUsers?retryWrites=true&w=majority"
);

// Register //
server.post("/registieren", async (req, res) => {
  try {
    const user = await model.create({
      vorname: req.body.vorname,
      nachname: req.body.nachname,
      email: req.body.email,
      userName: req.body.userName,
      password: req.body.password,
    });
    res.json({ status: "ok 200" });
    console.log(req.body);
  } catch (err) {
    res.json({ status: "error 400" });
    console.log(err);
  }
});

// Login //
server.post("/login", async (req, res) => {
  const check = await model.findOne({
    userName: req.body.userName,
    password: req.body.password,
  });
  if (check) {
    const token = jwt.sign(
      {
        userName: check.userName,
        password: check.password,
      },
      "secretwww"
    );
    console.log(token);
    res.send({
      status: 200,
      user: token,
    });
  } else {
    res.send({ status: 400 });
  }
});

// Payment //
server.post("/payment", (req, res) => {
  stripe.charges.create(
    {
      source: req.body.tokenId,
      amount: req.body.amount,
      currency: "eur",
    },
    async (stripeErr, stripeRes) => {
      if (stripeErr) {
        res.status(500).send(stripeErr);
      } else {
        res.status(200).send(stripeRes);
        const produkte = JSON.stringify(req.body.produkte);
        const user = req.body.user;
        const mailOptions = {
          from: "slhkhalidi@gmail.com",
          to: "slhkhalidi@gmail.com",
          subject: "Neue Bestellung Test",
          html: `
              <h1>Test Bestellung</h1>
               <ul> 
                <li><span>Username: </span>${user.card.name}</li>   
                <li><span>Straße: </span>${user.card.address_line1}</li>          
                <li><span>Platz: </span>${user.card.address_zip}</li>          
                <li><span>Stadt: </span>${user.card.address_city}</li>          
                <li><span>Email: </span>${user.email}</li>          
                <li><span>Produkte: </span>${produkte}</li>          
               </ul>
              `,
        };
        transporter.sendMail(mailOptions, (err, succ) => {
          if (err) {
            console.log(err);
          } else {
            console.log(user);
          }
        });
      }
    }
  );
});
