const mongoose = require("mongoose");

// This is our Schema for MongoDB //
const User = new mongoose.Schema({
    vorname: {type: String, required:true},
    nachname: {type: String, required:true},
    email: {type: String, required:true, unique:true},
    userName: {type: String, required:true, unique: true},
    password: {type: String, required:true, unique: true}
}, {collection: "users"})


const userData = mongoose.model("userData", User);

module.exports = userData; 